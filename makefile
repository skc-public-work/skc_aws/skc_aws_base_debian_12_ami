.PHONY: env_test validate build cleanbuid push_to_nexus

include .env
export

env_test:
	@echo $(value AWS_ACCESS_KEY_ID)
	@echo $(value AWS_SECRET_ACCESS_KEY)
	@echo $(value AWS_DEFAULT_REGION)

init:
	packer init .

validate:
	packer validate .

test_validate:
	packer validate -var-file=variables.pkr.hcl skc-test.pkr.hcl 

test_build:
	packer build -var-file=variables.pkr.hcl skc-test.pkr.hcl 

build:
	packer build .

cleanbuid:
	rm -rf ./build
