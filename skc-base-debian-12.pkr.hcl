
packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}


source "amazon-ebs" "skc_debian_12" {
  ami_name      = "skc_base_debian_12"
  instance_type = "t3.micro"
  source_ami    = "ami-0ec3d9efceafb89e0"
  ssh_username  = "admin"
  region        = "us-east-2"
    associate_public_ip_address = true
  tags = {
    Name = "skc_debian_12"
  }
}


build {
  sources = [
    "source.amazon-ebs.skc_debian_12"
  ]

  provisioner "shell" {
    #inline = ["sudo apt install -y python3"]
    inline = ["sudo apt-get update", "sudo apt-get upgrade -y", "python3 --version", "sudo apt install -y ansible"]
  }

  provisioner "ansible-local" {
    playbook_file   = "./ansible/build.yml"
    role_paths      = [
      "./ansible/roles/skc-print_os_release",
      "./ansible/roles/skc-update-os",
      "./ansible/roles/skc-add-users",
     ]
  }
}
